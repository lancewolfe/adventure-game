const express = require('express');
const session = require('express-session');

const port = process.env.PORT || 3000;

const app = express();

app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(
  session({
    secret: 'Ooga booga',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false },
  })
);

const possibleArtifacts = ['potion-drink', 'potion-keep', 'mirror', 'blades'];
const possibleGuardians = ['unicorn', 'phoenix', 'hellhound']; // Add genie?
const possibleWeapons = ['sword', 'bow', 'staff'];
const possibleDirections = ['left', 'forward', 'right']; // Add down?
const possibleOpponents = [
  'knight',
  'wolves',
  'ghost',
  'snakes',
  'golem',
  'wizard',
  'sea-serpent',
  'sphinx',
  'wereshark',
];
// Secrets will be the first param only, like artifacts
const possibleSecrets = ['secret-trapdoor']; // Make this riskier

// Root route
// app.get('/', (req, res) => {
//   res.render('start');
// });
app.get('/', (req, res) => {
  res.render('index');
});

app.post('/signup', (req, res) => {
  const valid_users = [
    { name: 'sue', password: 'sue' },
    { name: 'joe', password: 'joe' },
    { name: 'sam', password: 'sam' },
  ];

  const user = req.body.username;
  const pass = req.body.password;

  const found_user = valid_users.find(u => {
    return u.name === user && u.password === pass;
  });
  if (found_user) {
    req.session.username = user;
    res.render('start', {
      my_user: found_user.name,
    });
  } else {
    req.session.destroy(() => console.log('User reset'));
    res.redirect('/?reason=invalid_user');
  }
});

// Authentication (Authorization?) middleware attempt

app.get('/*', (req, res, next) => {
  if (req.session.username) {
    next();
  } else {
    res.redirect('/');
  }
});

app.get('/begin', (req, res) => {
  /* if (req.session && req.session.username) { */
  res.render('artifact');
  /* } else {
    res.redirect('/');
  } */
});

app.get('/death', (req, res) => {
  res.render('death');
});

app.get('/survival', (req, res) => {
  res.render('survival');
});

app.get('/too-long', (req, res) => {
  res.render('too-long');
});

// In reverse chronological order of choices made
app.get('/:artifact/:guardian/:weapon/:direction/:opponent', (req, res) => {
  if (
    possibleArtifacts.includes(req.params.artifact) &&
    possibleGuardians.includes(req.params.guardian) &&
    possibleWeapons.includes(req.params.weapon) &&
    possibleDirections.includes(req.params.direction) &&
    possibleOpponents.includes(req.params.opponent)
  ) {
    // MOVE AS MUCH OF THIS LOGIC AS POSSIBLE TO RELEVANT EJS
    // However, then how can I pass params as in redirect()?
    if (
      req.params.artifact === 'potion-keep' &&
      req.params.opponent === 'golem'
    ) {
      res.render('victory', { cause: 'potion' });
    }
    if (req.params.guardian === 'unicorn') {
      if (req.params.opponent === 'ghost') {
        res.render('victory', { cause: 'undead' });
      } else if (req.params.opponent === 'golem') {
        res.render('defeat', { cause: 'unbreakable' });
      }
    } else if (req.params.guardian === 'hellhound') {
      if (req.params.opponent === 'wolves') {
        res.render('victory', { cause: 'canine' });
      } else if (req.params.opponent === 'sphinx') {
        res.render('victory', { cause: 'sic-em' });
      }
    } else if (
      req.params.guardian === 'phoenix' &&
      req.params.opponent === 'sea-serpent'
    ) {
      res.render('defeat', { cause: 'extinguished' }); // HTTP header error?
    }
    if (req.params.weapon === 'staff' && req.params.opponent === 'wizard') {
      res.render('victory', { cause: 'trade' });
    } else {
      res.render('combat', {
        // Possibly excessive params
        artifact: req.params.artifact,
        guardian: req.params.guardian,
        weapon: req.params.weapon,
        direction: req.params.direction,
        opponent: req.params.opponent,
      });
    }
  } else {
    res.render('lost');
  }
});

app.get('/:artifact/:guardian/:weapon/:direction', (req, res) => {
  if (
    possibleArtifacts.includes(req.params.artifact) &&
    possibleGuardians.includes(req.params.guardian) &&
    possibleWeapons.includes(req.params.weapon) &&
    possibleDirections.includes(req.params.direction)
  ) {
    res.render('opponent', {
      artifact: req.params.artifact,
      guardian: req.params.guardian,
      weapon: req.params.weapon,
      direction: req.params.direction,
    });
  } else {
    res.render('lost');
  }
});

app.get('/:artifact/:guardian/:weapon', (req, res) => {
  if (
    possibleArtifacts.includes(req.params.artifact) &&
    possibleGuardians.includes(req.params.guardian) &&
    possibleWeapons.includes(req.params.weapon)
  ) {
    if (req.params.guardian === 'hellhound' && req.params.weapon === 'staff') {
      res.render('defeat', { cause: 'bad-dog' });
    } else {
      res.render('direction', {
        artifact: req.params.artifact,
        guardian: req.params.guardian,
        weapon: req.params.weapon,
      });
    }
  } else {
    res.render('lost');
  }
});

app.get('/:artifact/:guardian', (req, res) => {
  if (
    possibleArtifacts.includes(req.params.artifact) &&
    possibleGuardians.includes(req.params.guardian)
  ) {
    res.render('weapon', {
      artifact: req.params.artifact,
      guardian: req.params.guardian,
    });
  } else {
    res.render('lost');
  }
});

app.get('/:artifact', (req, res) => {
  if (
    possibleArtifacts.includes(req.params.artifact) ||
    possibleSecrets.includes(req.params.artifact)
  ) {
    if (req.params.artifact === 'potion-drink') {
      res.render('defeat', { cause: 'poison' });
    } else if (req.params.artifact === 'secret-trapdoor') {
      res.render('victory', { cause: 'trapdoor' });
    } else {
      res.render('guardian', { artifact: req.params.artifact });
    }
  } else {
    res.render('lost');
  }
});

// Wildcard route isn't really necessary here

app.listen(port, () => {
  console.log(`Server is running on port ${port}...`);
});
